<?php
require_once '../src/autoload.php';
$countryFlags = new \holodilnik\h1\flags();
$countryFlags->loadFromJSON('flags2.json');
$url_region_countries_order = "";
if (isset($_GET['sort'])) {
    if (isset($_GET['order_countries'])) {
        $url_region_countries_order = '&order_countries='.$_GET['order'];
    }
    switch($_GET['sort']){
        case 'country':
            $countryFlags->sortByCountry($_GET['order']);
            $url_region_countries_order = '&order_countries='.$_GET['order'];
            break;
        case 'region':
            $order_countries = isset($_GET['order_countries']) ? $_GET['order_countries'] : false;
            $countryFlags->sortByRegion($_GET['order'], $order_countries);
            break;
    }
}
?>
<!DOCTYPE html>
<html lang="">
	<head>
		<title>Тестовое задание</title>
		<meta charset="UTF-8">
		<meta name=viewport content="width=device-width, initial-scale=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="/css/vendor/bootstrap.min.css">
        <link rel="stylesheet" href="/css/vendor/font-awesome.min.css">
        <link rel="stylesheet" href="/css/1.css">
	</head>
	<body>
		<div class="container">
			<div class="row">
                <div class="col-sm-8">
                    <h1><a href="/1/">Флаги</a></h1>
                </div>
                <div class="col-sm-4 text-right">
                    <button type="button" class="btn btn-info" id="renew">Обновить</button>
                    <div class="renew hidden"><i class="fa fa-refresh fa-spin"></i> Идет обновление. Может занять около минуты.</div>
                    <div class="done hidden"><i class="fa fa-check"></i> Готово. Перезагружаем страницу...</div>
                </div>
			</div>
			<div class="row">
                <?php if(count($countryFlags->flagsArray)) : ?>
				<table class="table table-bordered table-hover">
					<thead>
						<tr>
                            <th>Флаг</th>
							<th>
                                Страна
                                <a href="/1/index.php?sort=country&order=asc" class=""><i class="fa fa-sort-alpha-asc"></i></a>
                                <a href="/1/index.php?sort=country&order=desc" class=""><i class="fa fa-sort-alpha-desc"></i></a>
                            </th>
							<th>
                                Регион
                                <a href="/1/index.php?sort=region&order=asc<?=$url_region_countries_order?>" class=""><i class="fa fa-sort-alpha-asc"></i></a>
                                <a href="/1/index.php?sort=region&order=desc<?=$url_region_countries_order?>" class=""><i class="fa fa-sort-alpha-desc"></i></a>
                            </th>
							<th>Дата принятия</th>
							<th>Пропорции</th>
						</tr>
					</thead>
					<tbody>
                    <?php foreach ($countryFlags->flagsArray as $countries) : ?>
                        <tr>
                            <td class="text-center">
                                <div class="flag_img">
                                    <div class="wave"></div>
                                    <img src="<?=$countries['img']?>">
                                </div>
                            </td>
                            <td><?=$countries['name']?></td>
                            <td><?=$countries['region']?></td>
                            <td><?=$countries['date']?></td>
                            <td><?=$countries['proportions']?></td>
                        </tr>
                    <?php endforeach; ?>
					</tbody>
				</table>
                <?php else : ?>
                <p class="alert alert-info text-center">Обновите данные с сервера</p>
                <?php endif; ?>
			</div>
			<div class="row">
                <?php  ?>
			</div>
		</div>
        
		<!-- jQuery -->
		<script src="/js/vendor/jquery.min.js"></script>
		<!-- Bootstrap JavaScript -->
		<script src="/js/vendor/bootstrap.min.js"></script>
		<script src="/js/1.js"></script>
	</body>
</html>