<?php
require_once '../src/autoload.php';

set_time_limit(200);
error_reporting(0);

$flagsParser = new \holodilnik\h1\flagsParser();
$flagsParser->parseAll();
$json_flags_file = $flagsParser->saveToJSON('flags2.json');
echo json_encode([
    'error' => false
]);
