--
-- Table structure for table `nested_categories`
--

DROP TABLE IF EXISTS `nested_categories`;
CREATE TABLE IF NOT EXISTS `nested_categories` (
  `CategoryID` int(11) NOT NULL DEFAULT '0',
  `lft` int(11) NOT NULL,
  `rgt` int(11) NOT NULL,
  `lvl` int(11) NOT NULL,
  `CategoryIDRef` int(11) NOT NULL DEFAULT '0',
  `CategoryName` varchar(100) CHARACTER SET cp1251 NOT NULL,
  `CategoryDescription` text CHARACTER SET cp1251 NOT NULL,
  `CategoryAlias` varchar(100) CHARACTER SET cp1251 NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;