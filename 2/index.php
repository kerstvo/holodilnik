<?php
require_once '../src/autoload.php';

$categories = new \holodilnik\h2\categories();
$categories->getCatalog();
$category = null;
$search = "";
if (isset($_GET['category'])) {
    $search = trim($_GET['category']);
    if (isset($_GET['subcategory']) && !empty($_GET['subcategory'])) {
        $search = trim($_GET['subcategory']);
    }
    $category = $categories->getCategoryInfo($search);
}
?>
<!DOCTYPE html>
<html lang="">
<head>
    <title>Тестовое задание</title>
    <meta charset="UTF-8">
    <meta name=viewport content="width=device-width, initial-scale=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="/css/vendor/bootstrap.min.css">
    <link rel="stylesheet" href="/css/vendor/font-awesome.min.css">
    <link rel="stylesheet" href="/css/2.css">
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-sm-8">
            <h1><a href="/2/">Категории</a></h1>
        </div>
        <div class="col-sm-4 text-right">
        </div>
    </div>

    <div class="row">
        <div class="col-sm-6">
            <ul>
                <?php $categories->printTree($categories->categories); ?>
            </ul>
        </div>
        <div class="col-sm-6">
        <?php if ($category) : ?>
            <ol class="breadcrumb">
                <li><a href="/2/">Home</a></li>
                <?php foreach ($category['breadcrumbs'] as $crumb) : ?>
                    <li><a href="<?=$crumb['alias']?>"><?=$crumb['name']?></a></li>
                <?php endforeach; ?>
                <li class="active"><?=$category['info']['name']?></li>
            </ol>
            <div class="well"><?=$category['info']['description']?></div>
        <?php endif; ?>
        </div>
    </div>
</div>

<!-- jQuery -->
<script src="/js/vendor/jquery.min.js"></script>
<!-- Bootstrap JavaScript -->
<script src="/js/vendor/bootstrap.min.js"></script>
<script src="/js/2.js"></script>
</body>
</html>