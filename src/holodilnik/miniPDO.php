<?php

namespace holodilnik;


class miniPDO
{
    static private $dbInstance;

    protected function __construct(){}

    public static function getDbInstance()
    {
        if (!self::$dbInstance) {
            try {
                $username = 'root';
                $password = '123';
                $dsn = 'mysql:host=localhost;dbname=hol_categories';
                $options = array(
                    \PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
                );

                self::$dbInstance = new \PDO($dsn, $username, $password, $options);
            } catch (\PDOException $e) {
                die("PDO CONNECTION ERROR: " . $e->getMessage() . "<br/>");
            }
        }
        return self::$dbInstance;
    }
} 