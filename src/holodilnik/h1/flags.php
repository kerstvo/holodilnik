<?php
namespace holodilnik\h1;

class flags
{
    public $flagsArray;

    public function __construct()
    {
        $this->flagsArray = [];
    }

    public function add($name, $region, $date, $proportions, $img)
    {
        return $this->flagsArray[] = [
            'name' => $name,
            'region' => $region,
            'date' => $date,
            'proportions' => $proportions,
            'img' => $img,
        ];
    }

    public function loadFromJSON($jsonname)
    {
        $fp = null;
        if (file_exists($jsonname)) {
            $fp = fopen($jsonname, 'r');
        }
        if(!$fp) return;

        $json_flags = json_decode(fread($fp, filesize($jsonname)));
        fclose($fp);

        foreach($json_flags as $flag) {
            $this->flagsArray[] = [
                'name'  => $flag->country,
                'region'  => $flag->region,
                'date'  => $flag->date,
                'proportions'  => $flag->proportions,
                'img'  => $flag->img_src,
            ];
        }
    }

    public function sortByCountry($asc_country = SORT_ASC)
    {
        $asc_country = $asc_country == 'desc' ? SORT_DESC : SORT_ASC;

        $this->flagsArray = $this->array_orderby($this->flagsArray, 'name', $asc_country);
        return $this->flagsArray;
    }

    public function sortByRegion($asc_region = SORT_ASC, $asc_country = SORT_ASC)
    {
        $asc_region = $asc_region == 'desc' ? SORT_DESC : SORT_ASC;

        if ($asc_region && $asc_country) {
            $asc_country = $asc_country == 'desc' ? SORT_DESC : SORT_ASC;

            $this->flagsArray = $this->array_orderby($this->flagsArray, 'region', $asc_region, 'name', $asc_country);
        }
        else {
            $this->flagsArray = $this->array_orderby($this->flagsArray, 'region', $asc_region);
        }
        return $this->flagsArray;
    }

    private function array_orderby()
    {
        $args = func_get_args();
        $data = array_shift($args);
        foreach ($args as $n => $field) {
            if (is_string($field)) {
                $tmp = array();
                foreach ($data as $key => $row)
                    $tmp[$key] = $row[$field];
                $args[$n] = $tmp;
            }
        }
        $args[] = &$data;
        call_user_func_array('array_multisort', $args);
        return array_pop($args);
    }
}
