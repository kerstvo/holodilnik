<?php

namespace holodilnik\h1;

class flagsParser
{
    CONST DOMAIN = 'http://flags.org.ru/';
    CONST URL = 'index.php';
    CONST XPATH_FLAGS               = '//table/tr[5]/td/table/tr/td[2]//strong//a';
    CONST XPATH_COUNTRY_REGION      = '//table/tr/td[3]/table/tr[1]/td/div/font/strong';
    CONST XPATH_COUNTRY_FLAG_IMG    = '//table/tr[2]/td/p/font[1]//img/@src';
    CONST XPATH_COUNTRY_DATE        = '//table/tr[2]/td/p/font[1]/strong/font';
    CONST XPATH_COUNTRY_PROPORTIONS = '//table/tr[3]/td/p/font/strong';

    private $ch;

    protected $domObj;
    protected $flagsUrls;

    protected $flags;

    public function __construct($xpath = false)
    {
        $this->flags = [];

        $this->domObj = new \DOMDocument;

        $html = $this->getUrlContent(self::DOMAIN.self::URL);

        libxml_use_internal_errors(true);
        $this->domObj->loadHTML($html);
    }

    public function parseCountry($country_name, $url)
    {
        $html = $this->getUrlContent($url);

        $this->domObj->loadHTML($html);
        $xpath = new \DOMXPath($this->domObj);

        $date = $xpath->query(self::XPATH_COUNTRY_DATE);
        if ($date->length) {
            $date = str_replace("принят ", "", $this->removeStuff($date->item(0)->nodeValue));
        }
        else {
            $date = "";
        }

        $proportions = $xpath->query(self::XPATH_COUNTRY_PROPORTIONS);
        if ($proportions->length) {
            $proportions = str_replace("соотношение сторон ", "", $this->removeStuff($proportions->item(0)->nodeValue));
        }
        else {
            $proportions = "";
        }

        $img = $xpath->query(self::XPATH_COUNTRY_FLAG_IMG);
        if ($img->length) {
            $img = $this->removeStuff($img->item(0)->nodeValue);
            $imgname = pathinfo($img);
            $img = $this->saveImg(self::DOMAIN.$img, $imgname['basename']);
        }
        else {
            $img = "";
        }

        $region = $xpath->query(self::XPATH_COUNTRY_REGION);
        if ($region->length) {
            $region = ucwords(strtolower($this->removeStuff($region->item(0)->nodeValue)));
        }
        else {
            $region = "";
        }

        $this->flags[] = [
            'country' => $country_name,
            'region' => $region,
            'date' => $date,
            'proportions' => $proportions,
            'img_src' => $img,
        ];
    }

    public function parseAll()
    {
        if (empty($this->flagsUrls)) {
            $this->getUrls();
        }
        foreach ($this->flagsUrls as $country_name => $url) {
            $this->parseCountry($country_name, $url);
        }
    }

    public function saveToJSON($jsonname)
    {
        $fp = fopen($jsonname,'w');
        fwrite($fp, json_encode($this->flags));
        fclose($fp);
        return $jsonname;
    }

    private function getUrls()
    {
        $xpath = new \DOMXPath($this->domObj);
        $nodes = $xpath->query(self::XPATH_FLAGS);
        foreach($nodes as $href) {
            $this->flagsUrls[$this->removeStuff($href->nodeValue)] = self::DOMAIN.$href->getAttribute('href');
        }
    }

    private function removeStuff($val)
    {
        return trim(preg_replace('!\s+!', ' ', str_replace("|", "", $val)));
    }

    private function getUrlContent($url)
    {
        $this->ch = curl_init();
        $timeout = 5;
        curl_setopt($this->ch, CURLOPT_URL, $url);
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->ch, CURLOPT_CONNECTTIMEOUT, $timeout);
        $html = curl_exec($this->ch);
        curl_close($this->ch);

        return $html;
    }

    private function saveImg($image_url, $name)
    {
        $this->ch = curl_init($image_url);
        curl_setopt($this->ch, CURLOPT_HEADER, 0);
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->ch, CURLOPT_BINARYTRANSFER,1);
        $jpg = curl_exec($this->ch);
        curl_close($this->ch);

        $name = 'img/'.$name;
        $fp = fopen(__DIR__.'/../../../1/'.$name,'w');
        fwrite($fp, $jpg);
        fclose($fp);

        return $name;
    }
}
