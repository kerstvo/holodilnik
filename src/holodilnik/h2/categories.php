<?php
namespace holodilnik\h2;

use holodilnik\miniPDO;

class categories
{
    private $lft;
    private $lvl;

    protected $dbh;

    public $breadcrumbs;
    public $categories;
    public $query_count;

    public function __construct() {
        $this->dbh = miniPDO::getDbInstance();

        $this->categories = [
            'children' => [],
            'params' => [
                'CategoryName' => 'Home',
                'CategoryID' => 0,
                'CategoryAlias' => 'home'
            ],
            'nested' => [
                'lft' => 0,
                'rgt' => 0,
                'lvl' => 0,
            ]
        ];
        $this->breadcrumbs = [];
        $this->query_count = 0;

    }

    private function _getCatalogTree(&$result, $cat_id = 0)
    {
        $this->lvl++;
        $query = $this->dbh->prepare("SELECT * FROM categories where CategoryIDRef = :id");
        $query->execute([
            'id' => $cat_id
        ]);
        $this->query_count++;

        while ($row = $query->fetchAll(\PDO::FETCH_ASSOC)) {
            foreach ($row as $cat) {
                $result[$cat['CategoryID']]['params'] = $cat;
                $result[$cat['CategoryID']]['nested']['lft'] = $this->lft++;
                //$result[$cat['CategoryID']]['children'] = null;
                $result[$cat['CategoryID']]['nested']['lvl'] = $this->lvl;

                    $this->_getCatalogTree($result[$cat['CategoryID']]['children'], $cat['CategoryID']);

                $result[$cat['CategoryID']]['nested']['rgt'] = $this->lft++;
            }
//            $result[$cat_id]['nested']['rgt'] = $this->lft++;
        }
        $this->lvl--;
        return $result;
    }

    public function getCatalog()
    {
        $this->categories['nested']['lft'] = $this->lft++;
        $this->categories['nested']['lvl'] = $this->lvl;
        $this->_getCatalogTree($this->categories['children'], $this->categories['params']['CategoryID']);
        $this->categories['nested']['rgt'] = $this->lft++;
        return $this->categories;
    }

    private function _getBreadcrumbs(&$result, $cat_id)
    {
        $query = $this->dbh->prepare("SELECT
                                    parent.*
                                FROM categories parent
                                where parent.CategoryID = :id");
        $query->execute([
            'id' => $cat_id
        ]);
    //    $query_count++;
        $row = $query->fetch(\PDO::FETCH_ASSOC);
        $result[] = [
            'name' => $row['CategoryName'] ? $row['CategoryName'] : 'home',
            'alias' => $row['CategoryAlias'] ? $row['CategoryAlias'] : '/2/',
            ];
        if ($row['CategoryIDRef'] > 0) {
            $this->_getBreadcrumbs($result, $row['CategoryIDRef']);
        }
        return $result;
    }

    public function getCategoryInfo($alias)
    {
        $query = $this->dbh->prepare("SELECT
                                    child.*
                                FROM categories child
                                where child.CategoryAlias = :alias");
        $query->execute([
            'alias' => $alias
        ]);
        //    $query_count++;
        $row = $query->fetch(\PDO::FETCH_ASSOC);
        $result = [
            'info' => [
                'name' => $row['CategoryName'],
                'id' => $row['CategoryID'],
                'alias' => $row['CategoryAlias'],
                'description' => $row['CategoryDescription'],
            ],
            'breadcrumbs' => [],
        ];
        if ($row['CategoryIDRef'] > 0) {
            $this->_getBreadcrumbs($result['breadcrumbs'], $row['CategoryIDRef']);
        }
        $result['breadcrumbs'] = array_reverse($result['breadcrumbs']);
        return $result;
    }

    function printTree($tree, $parent_url = '')
    {
        echo "<ul>";
        if ($parent_url) {
            $parent_url .= "/";
        }
        foreach ($tree['children'] as $key => $child) {
//            echo "<li>{$child['params']['CategoryID']}: {$child['nested']['lft']} - {$child['nested']['rgt']} / {$child['nested']['lvl']} &mdash; <a href=\"/2/$parent_url{$child['params']['CategoryAlias']}\">{$child['params']['CategoryName']}</a>";
            echo "<li><a href=\"/2/$parent_url{$child['params']['CategoryAlias']}\">{$child['params']['CategoryName']}</a>";
            if ($child['children']) {
                $this->printTree($child, $child['params']['CategoryAlias']);
            }
            echo "</li>";
        }
        echo "</ul>";
    }

    public function resetTree()
    {
        $this->tree = [];
    }

    public function resetBreadcrumbs()
    {
        $this->breadcrumbs = [];
    }
}