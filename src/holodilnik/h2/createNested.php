<?php

namespace holodilnik\h2;


use holodilnik\miniPDO;

class createNested {
    private $lft;
    private $rgt;
    private $lvl;
    private $db;
    CONST SRC_TABLE = 'categories';
    CONST DST_TABLE = 'nested_categories';

    public $queries;

    public function __construct()
    {
        $this->db = miniPDO::getDbInstance();
        $this->a_level = 0;
        $this->rgt = $this->lft = $this->lvl = 0;
    }

    private function getCatalogTree($cat_id)
    {
        $this->lvl++;
        $query = $this->db->prepare("SELECT * FROM ".self::SRC_TABLE." WHERE CategoryIDRef = :id");
        $query->execute([
            'id' => $cat_id
        ]);

        while ($row = $query->fetchAll(\PDO::FETCH_ASSOC)) {
            foreach ($row as $cat) {
                $this->queries[$cat['CategoryID']] = [
                    'CategoryID'            => $cat['CategoryID'],
                    'lft'                   => $this->lft++,
                    'rgt'                   => '',
                    'lvl'                   => $this->lvl,
                    'CategoryIDRef'         => $cat['CategoryIDRef'],
                    'CategoryName'          => $this->db->quote($cat['CategoryName']),
                    'CategoryDescription'   => $this->db->quote($cat['CategoryDescription']),
                    'CategoryAlias'         => $this->db->quote($cat['CategoryAlias'])
                    ];

                $this->getCatalogTree($cat['CategoryID']);

                $this->queries[$cat['CategoryID']]['rgt'] = $this->lft++;
            }
        }
        $this->lvl--;
        return $cat_id;
    }

    private function getInsertQueries($init_id)
    {
        $this->getCatalogTree($init_id);
        $values = [];
        $rez = "INSERT INTO ".self::DST_TABLE." (CategoryID, lft, rgt, lvl, CategoryIDRef, CategoryName, CategoryDescription, CategoryAlias) VALUES ";
        foreach ($this->queries as $q) {
            $values[] = $rez."(".implode(", ", $q).")";
        }
//        return $rez.implode(",\n", $values).";";
        return implode(";\n", $values).";";

    }

    public function transform($init_id = 0)
    {
        return $sql = $this->getInsertQueries($init_id);
    }
}