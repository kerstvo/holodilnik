(function () {
    $('a[href="'+window.location.pathname+window.location.search+'"]').addClass('current');

	$('#renew').on('click', function(){
        var $this = $(this),
            $renew = $this.siblings('.renew'),
            $done = $this.siblings('.done');

        $renew.removeClass('hidden', 400);
        $.get("update.php", function(data){
                $renew.addClass('hidden', 400);
                $done.removeClass('hidden', 400).delay(3000).queue(function() {
                    $(this).addClass('hidden');
                    $(this).dequeue();
                    location.reload(true);
                });
            }, "json")
            .always(function(){
            });
	});
})();